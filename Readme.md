

# Salesman

Desculpe-me o nome nada criativo. Este é o projeto solicitado pela Engeplus, seguindo os
seguintes requisitos funcionais:

>Uma empresa de tecnologia possui a necessidade de controlar as vendas e comissões de seus vendedores.
>Esta empresa comercializa produtos e serviços, para produtos a comissão é de 10% e serviços 25%.
>Mas para vendedores contratados há mais de 5 anos a porcentagem de comissão é de 30%.
>Apenas usuários autenticados podem acessar o sistema
>Ao realizar uma venda deve ser calculada a comissão
>Uma venda pode conter mais de um produto ou serviço
>Programação orientada a objetos
>Testes automatizados
>Layout responsivo
>Uso de frameworks e bibliotecas


E os seguintes requisitos não funcionais:

>PHP
>Banco de dados MySQL

# Minha stack

## Backend

O SGBD utilizado foi o Mysql, como solicitado. Se eu tivesse de escolher um banco relacional, 
provavelmente escolheria este mesmo. Para um projeto menor, um SqLite, talvez.


Utilizei o PHP7 no paradigma orientado à objetos, visto este ser o paradigma 
que melhor me adapto, utilizando o PHP. 


Utilizo o framework Slim na versão 3 para modelar as rotas e utilizar seus objetos para 
lidar com request, responses e status.


Na parte de persistência, foi feito uso do Eloquent (o ORM do Laravel) para a modelagem,
acesso e gravação de dados no banco. Eu tenho muito apreço pela praticidade e simplicidade 
dos query builders. No entanto, o Eloquent permite modelagem e possui extensões interessantes.
Apesar de não ser um query builder, o Eloquent é muito semelhante a um na hora de recuperar os
dados.


Foi configurado o Twig na camada view, mas acabei não fazendo muito uso dele devido ao fato de
que a aplicação desenvolvida é uma SPA.


Para as migrations do banco de dados, eu utilizei o Phinx.


## Frontend


Visto esta página ser uma SPA, o PHP renderiza a página apenas uma vez. Sendo assim, as telas são
escritas em Javascript (ECMAScript 6) e estilizadas em CSS com marcação HTML5.


Um framework javascript minimalista (1kb de código) foi configurado no frontend. Tal framework é o
Hyperapp, que renderiza os elementos e manipula o estado da tela com fluxo unidirecional de dados.


Para as rotas, não usei nada de especial, nem mesmo uma lib de router para Hyperapp que existe. O
que eu fiz foi apenas utilizar o window.location.hash do navegador. Para cada âncora direfente, eu 
mudo a tela à ser exibida. Para isso, eu utilizo o evento hashchange da window.


Foi feito uso do framework CSS mais famoso: O Twitter Bootstrap. A versão em questão é a 4.x (atual).
Entretanto, eu baixei uma versão com o estilo visual no padrão Material Design da Google.


Os testes da API são feitos com o supertest, uma ferramenta para fazer testes de API com o Node.


# Instalando

## Softwares necessários

 - S.O. Windows, Linux ou MacOS com Lampp ou Xampp Stack (há um Vagrantfile configurado no projeto)
 - Composer
 - NodeJS (apenas para build frontend e testes)

## Instalando

Primeiro, instale as dependências do PHP

```
composer install
```

Em seguida, instale as dependências de Frontend (não obrigatório)

```
npm install
```


Acesse o console do MySql (ou ferramenta de preferência) e crie um banco com o nome
salesman (pode mudar o nome e alterar no arquivo .env)

```
CREATE DATABASE salesman;
```

Edite o arquivo .env com os dados do seu banco de dados.
Agora, Use o Phinx para rodar as migrations e criar as tabelas.

```
php vendor/bin/phinx migrate
```


Use o Phinx para rodar as seeds e criar o usuário admin com as permissões e senha
```
php vendor/bin/phinx seed:run -s UserSeed
```


Se quaisquer modificações forem feitas no frontend, use o npm para transpilar o código.
Em desenvolvimento seria:
```
npm start
```


Depois de terminar as modificações no frontend, use o comando build para minimizar o javascript:
```
npm run build
```
